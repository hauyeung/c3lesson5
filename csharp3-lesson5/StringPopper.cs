﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace csharp3_lesson5
{
    class StringPopper : Popper
    {
        public StringPopper(Rectangle rectangle, Color color)
            : base(rectangle, color)
        {
            // Constructor to enable us to call base constructor
        }

        public override void Draw(Graphics graphics)
        {
            // Draw rectangle
            graphics.DrawString("a",new Font("Arial",20), new SolidBrush(Color.Red), new Point(base.X, base.Y));
        }

        public override bool Hit(Point point)
        {
            // Call abstract base class method
            return base.GetRectangle().Contains(point);
        }
    }
}
